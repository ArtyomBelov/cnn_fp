import os
import random
import pandas as pd
import numpy as np
from sklearn.utils import shuffle
from azino_png import watermark_photo
from azino_text import watermark_text

path = '/home/argus/Pictures/256_ObjectCategories/'
class_dir_list = os.listdir(path)
watermark_image_path = '/home/argus/keras_cnn/water_mark.png'
watermark_image_big_path = '/home/argus/keras_cnn/water_mark_big.png'

def get_imlist(path):
    """Возвращает список всех имен jpg файлов в каталоге"""
    return [os.path.join(path, f) for f in os.listdir(path) if f.endswith('.jpg')]

df_im_in_folder = pd.DataFrame(columns=['folder','imgs'])
for folder in class_dir_list:
    im_in_folder = path + folder
    im_list = get_imlist(im_in_folder)
    c_dic = {'folder': folder, 'imgs': [im_list]}
    c_list = pd.DataFrame(data=c_dic, index=None)
    df_im_in_folder = df_im_in_folder.append(c_list, sort=False, ignore_index=True)

print(df_im_in_folder)

# грузим строку из ДФ

df_len = len(df_im_in_folder['folder'].values)

for folder in range(df_len):
    df_str = df_im_in_folder.iloc[folder]
    folder_name = df_str[0]
    im_list = df_str[1]
    print('folder_name', folder_name)
    print('im_list', im_list)
    d = 0
    im_list_shuffled = shuffle(np.arange(len(im_list)))
    png_im_list = im_list_shuffled[:int(len(im_list)*0.1)]
    png_part_im_list = im_list_shuffled[int(len(im_list)*0.1):int(len(im_list)*0.2)]
    text_az_im_list = im_list_shuffled[int(len(im_list)*0.2):int(len(im_list)*0.3)]
    text_vu_im_list = im_list_shuffled[int(len(im_list)*0.3):int(len(im_list)*0.4)]
    print('png_im_list', png_im_list)
    position = (random.randrange(5, 150), random.randrange(5, 150))
    for i in png_im_list:
        im_path = im_list[i]
        watermark_photo(im_path, im_path[:-3]+'png', watermark_image_path, position)
        print('ОК png')

    for i in png_part_im_list:
        im_path = im_list[i]
        watermark_photo(im_path, im_path[:-3]+'png', watermark_image_big_path, position)
        print('ОК png')

    for i in text_az_im_list:
        im_path = im_list[i]
        text = 'azino777'
        watermark_text(im_path, im_path[:-3]+'png', text, position)
        print('ОК text')

    for i in text_vu_im_list:
        im_path = im_list[i]
        text = 'vulkan'
        watermark_text(im_path, im_path[:-3]+'png', text, position)
        print('ОК text')




