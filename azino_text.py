from PIL import Image
from PIL import ImageDraw
from PIL import ImageFont


def watermark_text(input_image_path,
                   output_image_path,
                   text,pos):
    try:
        photo = Image.open(input_image_path).convert('RGBA')
        txt = Image.new('RGBA', photo.size, (255, 255, 255, 10))

        # make the image editable
        drawing = ImageDraw.Draw(txt)
        font = ImageFont.truetype('SWEETDOU.TTF', 35)
        drawing.text(pos, text, fill=(0, 0, 0, 92), font = font)
        combined = Image.alpha_composite(photo, txt)
        # combined.show()
        # combined.convert('RGB')
        combined.save(output_image_path)
    except OSError:
        print('Ошибка тения файла')

if __name__ == '__main__':
    img = '/home/argus/video_fingerprint/iframe/tr_1/scr00030.png'
    # img = '/home/argus/Pictures/256_ObjectCategories/002.american-flag/002_0097.jpg'
    watermark_text(img,'watermarked.png', text='AZINO777', pos=(100, 100))