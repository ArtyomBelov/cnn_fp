from tensorflow.python.keras.preprocessing.image import ImageDataGenerator
from tensorflow.python.keras.models import Sequential
from tensorflow.python.keras.layers import Conv2D, MaxPooling2D
from tensorflow.python.keras.layers import Activation, Dropout, Flatten, Dense
from keras.callbacks import ModelCheckpoint
from keras.applications.resnet50 import ResNet50
from keras.applications.vgg16 import VGG16
from keras.models import Model
import os
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '2'

# Каталог с данными для обучения
train_dir = 'data/train'
# Каталог с данными для проверки
val_dir = 'data/valid'
# Каталог с данными для тестирования
test_dir = 'data/test'
# Размеры изображения
img_width, img_height = 256, 256
# Размерность тензора на основе изображения для входных данных в нейронную сеть
# backend Tensorflow, channels_last
input_shape = (img_width, img_height, 3)
# Количество эпох
epochs = 5
# Размер мини-выборки
batch_size = 100
# Количество изображений для обучения
nb_train_samples = 8413
# Количество изображений для проверки
nb_validation_samples = 1826
# Количество изображений для тестирования
nb_test_samples = 1936


# # создание модели
# model = Sequential()
# model.add(Conv2D(32, (3, 3), input_shape=input_shape))
# model.add(Activation('relu'))
# model.add(MaxPooling2D(pool_size=(2, 2)))
#
# model.add(Conv2D(32, (3, 3)))
# model.add(Activation('relu'))
# model.add(MaxPooling2D(pool_size=(2, 2)))
#
# model.add(Conv2D(64, (3, 3)))
# model.add(Activation('relu'))
# model.add(MaxPooling2D(pool_size=(2, 2)))
#
# model.add(Flatten())
# model.add(Dense(64))
# model.add(Activation('relu'))
# model.add(Dropout(0.5))

base_model = VGG16(weights='imagenet', input_shape=(256,256,3), include_top=False)
base_out = base_model.output
do = Dropout(0.5)(base_out)
softmax = Dense(257)(do)
result = Activation('softmax')(softmax)

model = Model(input=base_model.input, output=result)

model.compile(loss='categorical_crossentropy',
              optimizer='adam',
              metrics=['accuracy'])

# подгрузка датасета
datagen = ImageDataGenerator(rescale=1. / 255)

train_generator = datagen.flow_from_directory(
    train_dir,
    target_size=(img_width, img_height),
    batch_size=batch_size,
    class_mode='categorical')
val_generator = datagen.flow_from_directory(
    val_dir,
    target_size=(256, 256),
    batch_size=100,
    class_mode='categorical')
test_generator = datagen.flow_from_directory(
    test_dir,
    target_size=(256, 256),
    batch_size=100,
    class_mode='categorical')

сheckpoint = ModelCheckpoint('save/mnist-dense.hdf5',
                              monitor='val_acc',
                              save_best_only=True)

model.fit_generator(
    train_generator,
    steps_per_epoch=nb_train_samples // batch_size,
    epochs=5,
    validation_data=val_generator,
    validation_steps=nb_validation_samples // batch_size)

scores = model.evaluate_generator(test_generator, nb_test_samples // batch_size)
print("Аккуратность на тестовых данных: %.2f%%" % (scores[1]*100))


