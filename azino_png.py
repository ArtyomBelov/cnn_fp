from PIL import Image

def watermark_photo(input_image_path,
                    output_image_path,
                    watermark_image_path,
                    position):
    try:
        base_image = Image.open(input_image_path).convert('RGBA')
        watermark = Image.open(watermark_image_path).convert('RGBA')

        # add watermark to your image
        base_image.paste(watermark, position)
        # base_image.show()
        # base_image.convert('RGB')
        base_image.save(output_image_path)
    except OSError:
        print('Ошибка тения файла')


if __name__ == '__main__':
    img = '/home/argus/video_fingerprint/iframe/tr_1/scr00030.png'
    img2 = '00030_water_mark.png'
    watermark_photo(img, img2, 'water_mark_big.png', position=(250, 300))