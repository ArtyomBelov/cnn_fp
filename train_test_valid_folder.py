import os
import pandas as pd
import numpy as np
from sklearn.utils import shuffle
import cv2

path = '/home/argus/Pictures/256_ObjectCategories/'
class_dir_list = os.listdir(path)
watermark_image_path = '/home/argus/keras_cnn/water_mark.png'
watermark_image_big_path = '/home/argus/keras_cnn/water_mark_big.png'

def get_imlist(path):
    """Возвращает список всех имен jpg файлов в каталоге"""
    return [os.path.join(path, f) for f in os.listdir(path) if f.endswith('.png')]

df_im_in_folder = pd.DataFrame(columns=['folder','imgs'])
for folder in class_dir_list:
    im_in_folder = path + folder
    im_list = get_imlist(im_in_folder)
    c_dic = {'folder': folder, 'imgs': [im_list]}
    c_list = pd.DataFrame(data=c_dic, index=None)
    df_im_in_folder = df_im_in_folder.append(c_list, sort=False, ignore_index=True)

print(df_im_in_folder)

# грузим строку из ДФ

df_len = len(df_im_in_folder['folder'].values)

if not os.path.exists('data'):
    os.mkdir('data')
    os.mkdir('data/train')
    os.mkdir('data/test')
    os.mkdir('data/valid')

for folder in range(df_len):
    df_str = df_im_in_folder.iloc[folder]
    folder_name = df_str[0]
    im_list = df_str[1]

    if not os.path.exists('data/train/'+folder_name):
        os.mkdir('data/train/'+folder_name)
    if not os.path.exists('data/test/'+folder_name):
        os.mkdir('data/test/'+folder_name)
    if not os.path.exists('data/valid/'+folder_name):
        os.mkdir('data/valid/'+folder_name)

    lil = len(im_list)
    train_im_list = im_list[:int(lil * 0.7)]
    test_im_list = im_list[int(lil * 0.7):int(lil * 0.85)]
    valid_im_list = im_list[int(lil * 0.85):]

    def save_im_in_folder(image_list, folder_name, aim):
        d = 0
        ex = '.png'
        for i in image_list:
            try:
                im = cv2.imread(i)
                cv2.imwrite('data/' + aim + '/' + folder_name + '/'+ str(d) + ex, im)
                d += 1
                print('файл пересохранен')
            except OSError:
                print('Ошибка тения файла')

    save_im_in_folder(train_im_list, folder_name=folder_name, aim='train')
    save_im_in_folder(test_im_list, folder_name=folder_name, aim='test')
    save_im_in_folder(valid_im_list, folder_name=folder_name, aim='valid')

    print('folder_name', folder_name)
    print('im_list', im_list)